#!/usr/bin/env python
PKG='readusb'

import sys
import os
import rospy
import std_msgs
import roslib; roslib.load_manifest(PKG)
import numpy
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats
#from std_msgs.msg import string

filename=os.listdir('/media')
print filename

'''
File to be searched is sent through command line argument

filestring will contain the array of lattitude and longitude
In the array :
 	oddnumbered elements  : lattitude
	evennumbered elements : longitude
'''

filestring=[]
file_to_be_read='Float'


def talker():

	file_found=0

	pub=rospy.Publisher('gpsarray',numpy_msg(Floats),latch=True,queue_size=100)
	rospy.init_node('read_and_publish',anonymous=True)
	#boolpub=rospy.Publisher('check_for_if_data_is_subscribed',std_msgs.msg.Bool,queue_size=100)
	rate=rospy.Rate(10)

	
	'''
	code for going through the directory media and finding the right file in multiple plugged in usb stick
	'''
	for i in range(len(filename)):
		path='/media/'
		for usbdevice in os.listdir(path+filename[i]):
        		path=path+filename[i]
			if file_to_be_read in os.listdir(path+'/'+usbdevice):
                		path=path+'/'+usbdevice
				file_found=1
				f=open(path+'/'+file_to_be_read)
				break
		if file_found==0:
			continue
		else:
			#print path+'/'+file_to_be_read
			'''
			coverting into numpy array so as to print float32 array
			'''
			filestring=numpy.array(map(float,f.read().split()),dtype=numpy.float32)

#	print len(filestring)
			filestring=numpy.insert(filestring,0,len(filestring))
			pub.publish(filestring)
			rospy.loginfo(filestring)
			'''
			publishing over boolean topic : check_for_if_data_is_subscribed
			'''

			while not rospy.is_shutdown():
				if rospy.is_shutdown():
					break

			#rospy.loginfo('False')
			#boolpub.publish(False)

		#rospy.loginfo('True')
		#boolpub.publish(True)


try:
	talker()
except rospy.ROSInterruptException:
	pass
