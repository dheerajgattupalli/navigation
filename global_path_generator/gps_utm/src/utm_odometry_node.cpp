/*
 * Translates sensor_msgs/NavSat{Fix,Status} into nav_msgs/Odometry using UTM
 */
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <sensor_msgs/NavSatStatus.h>
#include <sensor_msgs/NavSatFix.h>
#include <gps_common/conversions.h>
#include <nav_msgs/Odometry.h>
#include <rospy_tutorials/Floats.h>
#include <gps_utm/Array.h>
using namespace gps_common;

static ros::Publisher array_pub;
void callback(const rospy_tutorials::FloatsConstPtr& fix) {
	double northing, easting;
	std::string zone;
	gps_utm::Array array;
	int len=(int)fix->data[0];
	ROS_INFO("%d",len);
	array.data.push_back(len);
	for(int i=1;i<len;i+=2){
		//Function to convert from gps to utm
		LLtoUTM(fix->data[i], fix->data[i+1], northing, easting, zone);
	//	ROS_ERROR("gps-->%lf  %lf\n",fix->data[i],fix->data[i+1]);
	//	ROS_ERROR("utm-->%lf  %lf\n",easting,northing);
		//Publishing the array with utm coordinates
		if (array_pub) {
			array.data.push_back(easting);
			array.data.push_back(northing);
		}
	}
	array_pub.publish(array);
	ROS_ERROR("DONE");
}

int main (int argc, char **argv) {
	ros::init(argc, argv, "gps_utm_node");
	ros::NodeHandle node;
	array_pub = node.advertise<gps_utm::Array>("/utmarray", 10);
	ros::Subscriber new_sub = node.subscribe("/gpsarray", 10, callback);
	ros::spin();
}
