#include <stdio.h>
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
using namespace geometry_msgs;
double sum,prev_x,prev_y,prev_z=0,initx,inity,initz;
double x[1000],y[1000],z[1000];
int i=1;
static ros::Publisher path_pub;
void path_finder(int start){
	Twist twist;
	double fx,prev_fx=0,prev_fy=0,psum,prod;
	int yi,yj;
	for(fx=x[start];fx<x[start+4]-0.01;fx=fx+0.01){
		psum=0;
		for(yi=start;yi<start+5;yi++){
			prod=1;
			for(yj=start;yj<start+5;yj++){
				if(yj!=yi){
					prod=prod*((fx-x[yj])/(x[yi]-x[yj]));
				}
			}
			psum=psum+y[yi]*prod;
		}
		twist.linear.x =sqrt(pow(fx-prev_fx,2)+pow(psum-prev_fy,2));
		twist.angular.z = atan2(psum,fx)-atan2(prev_fy,prev_fx);
		prev_fx=fx;
		prev_fy=psum;
		path_pub.publish(twist);
	}
}
void callback(const nav_msgs::OdometryConstPtr& odom){
	static double initx=odom->pose.pose.position.x;
	static double inity=odom->pose.pose.position.y;
	static double initz=odom->pose.pose.position.z;
	sum=odom->pose.pose.position.y-y[i-1]-inity;
	if(sum>5){
		x[i]=odom->pose.pose.position.x;
		y[i]=odom->pose.pose.position.y;
		z[i]=odom->pose.pose.position.z;
		x[i]=x[i]-initx;
		y[i]=y[i]-inity;
		z[i]=z[i]-initz;
		i++;
	if(i%5==0){
		path_finder(i-5);
		ROS_INFO("%lf\n",y[i-1]);
	}
	}
}
int main (int argc, char **argv) {
	ros::init(argc, argv, "curve_finder");
	ros::NodeHandle node;

	path_pub = node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 1000);
	ros::Subscriber odom_sub = node.subscribe("odom", 10, callback);
	ros::spin();
}
