#include <stdio.h>
#include <ros/ros.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <std_msgs/Float64MultiArray.h>
#include <nav_msgs/Odometry.h>
#include <math.h>
#include <gps_utm/Array.h>
using namespace std_msgs;
static ros::Publisher path_pub;
double x[10000],y[10000];
int t=0;
void path_find(int start,int end){
	start=start>0?start:0;
	std_msgs::Float64MultiArray xy;
	double fx,fx1,prod,psum;
	int yi,yj,yk,i=0;
	for(yk=start;yk<end-1;yk++){    
		for(fx=x[yk];abs(fx-x[yk+1])>0.01;){     //finding y coordinates with increment x by constant
			psum=0;
			//this is curve equation
			for(yi=start;yi<end;yi++){
				prod=1;
				for(yj=start;yj<end;yj++){
					if(yj!=yi){
						prod=prod*((fx-x[yj])/(x[yi]-x[yj]));
					}
				}
				psum=psum+y[yi]*prod;
			}
			xy.data.push_back(fx);
			xy.data.push_back(psum);
			printf("%lf %lf\n",fx,psum);
			fx=fx+((x[yk+1]-x[yk])*0.1);
		}
	}
	//publishing the array of coordinates
	path_pub.publish(xy);
}
void callback(const gps_utm::ArrayConstPtr& odomarray){
	int len=(int)odomarray->data[0];
	int i;
	int j=1;
	int k=0;
//	for(i=1;i<len;i+=2)
//		printf("%lf %lf\n",odomarray->data[i+1],odomarray->data[i+2]);
	for(i=0;i<len;i+=2){
		if(abs(x[j-1]-odomarray->data[i+1])>0.5){
		x[j]=odomarray->data[i+1];
		y[j]=odomarray->data[i+2];
		j++;
		if(j%5==0 && j!=0){
			path_find(j-6,j);
			k=j;
		}
	}
		if(i==len-2){
			path_find(k-1,j);
		}
}
}
int main (int argc, char **argv) {
	ros::init(argc, argv, "curve_finder");
	ros::NodeHandle node;
	path_pub = node.advertise<std_msgs::Float64MultiArray>("/odometry", 1000);
	ros::Subscriber odom_sub = node.subscribe("/odomarray", 10, callback);
	ros::spin();
}
