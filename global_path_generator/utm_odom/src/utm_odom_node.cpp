#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include "gps_utm/Array.h"
#include <geometry_msgs/PoseWithCovarianceStamped.h>
geometry_msgs::PointStamped impoint;
ros::Publisher pubu2o; 
double roll,pitch,yaw;	
void utmscallback(const gps_utm::Array::ConstPtr& msg)
{
	ROS_ERROR("HI");
	tf::TransformListener listen;
  	tf::StampedTransform transform;
  	int len=msg->data[0];
  	ROS_ERROR("%d",len);
  	gps_utm::Array array;
  	array.data.push_back(len);
  	for(int i=1;i<len;i+=2){
	impoint.header.frame_id = "utm";
	impoint.point.x = msg->data[i];
	impoint.point.y = msg->data[i+1];
	impoint.point.z = 0;
		impoint.header.stamp = ros::Time();
      tf::Quaternion utm_pose;
	try{
		geometry_msgs::PointStamped odompoint;
		listen.waitForTransform("utm", "odom_combined",ros::Time(0), ros::Duration(10.0));
		listen.transformPoint("odom_combined",impoint, odompoint);		
		printf("%lf %lf\n",odompoint.point.x,odompoint.point.y);

		array.data.push_back(odompoint.point.x);
		array.data.push_back(odompoint.point.y);		
	}
	catch(tf::TransformException& ex){
		ROS_ERROR("Received an exception trying to transform a point from \"utm\" to \"odom_combined\": %s", ex.what());
	}	
}
	pubu2o.publish(array);
}

int main(int argc, char** argv){
	ros::init(argc, argv, "utm2odompoints");
	//ROS_ERROR("HI DUDE");
	ros::NodeHandle node;
	ros::NodeHandle node1;
	pubu2o = node1.advertise<gps_utm::Array>("/odomarray", 100);
	ros::Subscriber imusub = node.subscribe("/utmarray", 100, utmscallback);
	ros::spin();

}